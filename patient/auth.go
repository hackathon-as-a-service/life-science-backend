package patient

import (
	"errors"

	"github.com/labstack/echo"

	tokengen "gitlab.com/gett-hackathon/life-science-backend/tokengen"
)

func (handler *Handler) AuthenticationMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		token := ctx.Request().Header.Get("Patient-Token")
		id, err := tokengen.HashToken(token)
		if err != nil {
			return errors.New("Unauthorized")
		}

		patient, err := handler.service.GetPatient(id)
		if err != nil {
			return errors.New("Unauthorized")
		}

		ctx.Set("patient", patient)
		return next(ctx)
	}
}
