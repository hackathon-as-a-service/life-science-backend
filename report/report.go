package report

import (
	"time"
)

type Medical struct {
}

type Disease struct {
	Creation   time.Time
	Diagnose   string `json:"diagnose" bson:"diagnose"`
	SubReports []*Disease
}
